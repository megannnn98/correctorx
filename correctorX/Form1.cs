﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace correctorX
{
    public partial class textCorrector : Form
    {
        public textCorrector()
        {
            InitializeComponent();

            ToolTip btn1ToolTip = new ToolTip();
            ToolTip checkBox1ToolTip = new ToolTip();
            ToolTip radioButton1ToolTip = new ToolTip();
            ToolTip checkBox2ToolTip = new ToolTip();

            /*
            btn1ToolTip2.SetToolTip(button1, "После точки, восклицэ или вопроса установить букву сделать большой");
            btn2ToolTip2.SetToolTip(button2, "Сократить пробелы длиннее одного символа");*/
            /*
            btn4ToolTip2.SetToolTip(button4, "Найти маленькие \"ооо\", \"ип\", \"инн\" и увеличить");
            btn5ToolTip2.SetToolTip(button5, "Пробелы после точек, запяых, скобок, перед скобками");
            btn6ToolTip2.SetToolTip(button6, "Удаление пробелов перед точкой, восклицэ и вопросом");
            */

            btn1ToolTip.SetToolTip(button1, "Все буквы в нижний регистр");
            checkBox1ToolTip.SetToolTip(checkBox1, "Разрешить обрабатывать и выводить автоматически взятый из буфера текст раз в секунду");
            radioButton1ToolTip.SetToolTip(radioButton1, "Индикатор обработки текста, мигает когда обработка закончена");
            checkBox2ToolTip.SetToolTip(checkBox2, "Разрешить вставлять обработанный текст в буффер обмена автоматически");
            
            radioButton1.Checked = false;
            checkBox1.Checked = false;

            MessageBox.Show("Чтобы включить обработку текста посавь галку на processionOn");
        }


        private void setBigChars()
        {
            string str = richTextBox1.Text;

            bigCharFirst(ref str);
            bigCharAfterPoint(ref str);

            richTextBox1.Text = str;
        }



        // ** Первый символ с большой буквы
        private void bigCharFirst(ref string str1)
        {
            char[] array = str1.ToCharArray();
            int j = 0;
            do
            {
                if (j+1 >= array.Length)
                {
                    break;
                }
            }
            while (false == isCharacter(str1[j++]));
            symToUpper(ref str1, --j);
        }
        private void symToUpper(ref string str, int i)
        {
            string sym = Convert.ToString(str[i]);
            sym = sym.ToUpper();

            str = str.Remove(i, 1);
            str = str.Insert(i, sym);
        }
        private void bigCharAfterPoint(ref string str)
        {
            char[] array = str.ToCharArray();

            for (UInt16 i = 0; i < array.Length; i++)
            {
                // ** Поиск точки или восклицэ
                // ** Когда нашли, докручиваем до ближайшего 
                // ** символа и делаем его большим
                if ((array[i] == '.') || (array[i] == '!') || (array[i] == '?')
                    || (array[i] == '\n'))
                {
                    int k = 0;
                    do {
                        // ** Если сидим в цикле очень долго
                        if (k++ >= array.Length) { break; }
                        if (i   >= array.Length) { break; }
                    }
                    while (false == isCharacter(array[i++]));

                    symToUpper(ref str, --i);
                }
            }
        }









        // ** Является ли принятый символ буквой?
        private bool isCharacter(char sym)
        {
            if ((sym >= 'a') && (sym <= 'z') ||
                (sym >= 'A') && (sym <= 'Z') || 
                (sym >= 'а') && (sym <= 'я') || 
                (sym >= 'А') && (sym <= 'Я'))
            {
                return true;
            }
            return false;            
        }















        private void toLower()
        {
            string str1 = richTextBox1.Text;

            str1 = str1.ToLower();

            richTextBox1.Text = str1;
        }
        private void deleteSpaces()
        {
            string str1 = richTextBox1.Text;
            char[] array = str1.ToCharArray();
            string tmp = "";

            // Сделал все пробелы одинарными
            for (UInt16 i = 0; i < array.Length; i++)
            {
                if (array[i] == ' ')
                {
                    tmp += Convert.ToString(array[i++]);
                    if (i >= array.Length) { break; }
                    while (array[i] == ' ')
                    {
                        i++;
                        if (i >= array.Length) { break; }
                    }
                }
                tmp += Convert.ToString(array[i]);
            }

            // --------------------------------------------------------
            string tmp1 = "";
            array = tmp.ToCharArray();
            for (UInt16 i = 0; i < array.Length; i++)
            {
                if (array[i] == ' ')
                {
                    i++;

                    if (i >= array.Length) { break; }
                    if (array[i] == '.')
                    {
                        tmp1 += Convert.ToString(array[i]);
                    }
                    else
                    {
                        i--;
                        tmp1 += Convert.ToString(array[i]);
                    }
                }
                else
                {
                    tmp1 += Convert.ToString(array[i]);
                }
            }


            richTextBox1.Text = tmp1;
        }

        private void insertSpaces()
        {
            string str1 = richTextBox1.Text;
            char[] array = str1.ToCharArray();
            string tmp = "";
            int i = 0;

            while(true)
            {
                if (i >= array.Length) { break; }
                if ((array[i] == ',') || (array[i] == '.') || (array[i] == ';'))
                {
                    tmp += array[i];
                    i++;
                    tmp += ' ';
                }
                else
                {
                    tmp += array[i];
                    i++;
                }
            }

            richTextBox1.Text = tmp;
        }
        private void setBigOOO()
        {
            string str = richTextBox1.Text;

            if (0 == findSubstrigAndUpper(ref str, " инн ")) { return; }
            if (0 == findSubstrigAndUpper(ref str, "инн ")) { return; }
            if (0 == findSubstrigAndUpper(ref str, "Инн ")) { return; }
            if (0 == findSubstrigAndUpper(ref str, " ип ")) { return; }
            if (0 == findSubstrigAndUpper(ref str, "ип ")) { return; }
            if (0 == findSubstrigAndUpper(ref str, "Ип ")) { return; }
            if (0 == findSubstrigAndUpper(ref str, " ооо ")) { return; }
            if (0 == findSubstrigAndUpper(ref str, "ооо ")) { return; }
            if (0 == findSubstrigAndUpper(ref str, "Ооо ")) { return; }
            if (0 == findSubstrigAndUpper(ref str, " зао ")) { return; }
            if (0 == findSubstrigAndUpper(ref str, "зао ")) { return; }
            if (0 == findSubstrigAndUpper(ref str, "Зао ")) { return; }
            if (0 == findSubstrigAndUpper(ref str, " нпп ")) { return; }
            if (0 == findSubstrigAndUpper(ref str, "нпп ")) { return; }
            if (0 == findSubstrigAndUpper(ref str, "Нпп ")) { return; }
            if (0 == findSubstrigAndUpper(ref str, " чоп ")) { return; }
            if (0 == findSubstrigAndUpper(ref str, "чоп ")) { return; }
            if (0 == findSubstrigAndUpper(ref str, "Чоп ")) { return; }
            if (0 == findSubstrigAndUpper(ref str, " жэк ")) { return; }
            if (0 == findSubstrigAndUpper(ref str, "жэк ")) { return; }
            if (0 == findSubstrigAndUpper(ref str, "Жэк ")) { return; }
        }






















        // ** Поиск нужной подстроки в строке и 
        // ** вставка ее в верхнем регистре
        private int findSubstrigAndUpper(ref string str, string substr)
        {
            int index;
            string substrUPPER;
            index = str.IndexOf(substr);
            if (-1 != index)
            {
                substr = str.Substring(index, substr.Length);
                substrUPPER = substr.ToUpper();
                str = str.Replace(substr, substrUPPER);
                
                richTextBox1.Text = str;
                return 0;
            }
            return -1;
        }










        void spaceAfterSym(char sym)
        {
            string str = richTextBox1.Text;

            // "sym" на " sym"
            for (int i = 0; i < str.Length - 1; i++)
            {
                if ((sym == str[i]) && (' ' != str[i - 1]))
                {
                    str = str.Insert(i, " ");
                }
            }
        }

        private void after()
        {
            string str = richTextBox1.Text;

            spaceAfterSym('(');
            spaceAfterSym(')');
            spaceAfterSym(',');
            spaceAfterSym('.');
            spaceAfterSym('!');
            spaceAfterSym('?');
            spaceAfterSym(':');

            // [\"] На [\" ] 
            for (int i = 0; i < str.Length - 1; i++)
            {
                if (('\"' == str[i]) && (' ' != str[i + 1]))
                {
                    str = str.Insert(i + 1, " ");
                }
            }

            richTextBox1.Text = str;
        }

        void spaceBeforeSym(char sym)
        {
            string str = richTextBox1.Text;
            // " sym" на "sym"
            for (int i = 0, j = 0; i < str.Length-1; i++)
            {
                if (sym == str[i])
                {
                    j = i - 1;
                    while (' ' == str[j])
                    {
                        str = str.Remove(j, 1);
                    }
                }
            }
            richTextBox1.Text = str;
        }


        private void before()
        {
            spaceBeforeSym('.');
            spaceBeforeSym('!');
            spaceBeforeSym('?');
            spaceBeforeSym(',');
            spaceBeforeSym(':');
            spaceBeforeSym(')');
        }

        private void button1_Click(object sender, EventArgs e)
        {
            toLower();
        }




        static String lastStr;
        private void timer1_Tick(object sender, EventArgs e)
        {
            // копируем буфер

            String buf = Clipboard.GetText();


            // если в буфере чтото новое и оно не ноль
            if ((buf != lastStr) &&
                (buf.Length != 0))
            {
                /*  и разрешено обработывать
                 *  выводим текст буфера на экран
                 *  выполняем обработку 
                 *  сохраняем последнее состояние строки после обработки
                 *  мигаем 100 мс
                 *  если галка установлена копируем текст в буфер обмена для вставки
                 */
                if (checkBox1.Checked == true)
                {
                    radioButton1.Checked = true;

                    richTextBox1.Text = buf;

                    deleteSpaces();
                    setBigOOO();
                    setBigChars();
                    after();
                    before();
                    deleteSpaces();
                    insertSpaces();
                    deleteSpaces();
                    
                    lastStr = richTextBox1.Text;


                    System.Threading.Thread.Sleep(100);
                    radioButton1.Checked = false;

                    if (checkBox2.Checked)
                    {
                        Clipboard.SetText(richTextBox1.Text);
                    }
                }
            }
            
        }



    }
}
